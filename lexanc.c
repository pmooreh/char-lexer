/* lex1.c         14 Feb 01; 31 May 12       */

/* This file contains code stubs for the lexical analyzer.
   Rename this file to be lexanc.c and fill in the stubs.    */

/* Copyright (c) 2001 Gordon S. Novak Jr. and
   The University of Texas at Austin. */

/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "token.h"
#include "lexan.h"

/* This file will work as given with an input file consisting only
   of integers separated by blanks:
   make lex1
   lex1
   12345 123    345  357
*/

/*
    returns the length of an array
*/
int arrLength(char** arr)
{
    int count = 0;

    while (arr[count] != 0)
    {
        //printf("%c\n", arr[count][0]);
        count++;
    }

    return count;
}


int indexInArr(char* string, char** strArr)
{
    int length = arrLength(strArr);

    //printf("XXX arr length: %d\n", length);

    for (int i = 0; i < length; i++)
    {
        if (strcmp(string, strArr[i]) == 0)
            return i;
    }
    return -1;
}

/*
    Removes the contents of a comment. The current position is the
    beginning of the comment, e.g., "{this is a comment}"

    commentType 0 -> '}' terminated comment
    commentType 1 -> '*)' terminated comment
*/
void removeComment(int commentType)
{
    // remove opening comments identifiers
    getchar();
    if (commentType == 1)
        getchar();

    // now traverse input until end of comment is reached! 
    int c = peekchar();
    int cc;

    while (c != EOF)
    {
        cc = peek2char();

        if (commentType == 0 && c == '}')
        {
            getchar();
            return;
        }
        else if (commentType == 1 && c == '*' && cc == ')')
        {
            getchar();
            getchar();
            return;
        }
        else
        {
            getchar();
        }

        c = peekchar();
    }
}

/* Skip blanks and whitespace.  Expand this function to skip comments too. */
void skipblanks()
{
    int c = peekchar();
    int cc;

    while (c != EOF)
    {
        cc = peek2char();

        // eliminate standard white space
        if (c == ' ' || c == '\n' || c == '\t')
        {
            getchar();
        }
        // capture single line comments
        else if (c == '{')
        {
            removeComment(0);
        }
        else if (c == '(' && cc == '*')
        {
            removeComment(1);
        }
        else
        {
            break;
        }

        c = peekchar();
    }
}

/* Get identifiers and reserved words */
TOKEN identifier(TOKEN tok)
{
    char c = peekchar();
    int charsAdded = 0;

    while (c != EOF && (CHARCLASS[c] == ALPHA || CHARCLASS[c] == NUMERIC))
    {
        c = getchar();
        if (charsAdded < 15)
        {
            tok->stringval[charsAdded] = c;
            charsAdded++;
        }

        c = peekchar();
    }

    tok->stringval[charsAdded + 1] = 0;

    /* 
        at this point, the string read in may be
            a reserved word
            an operator
            an identifier
        check each case!
    */
    // get the resvd word val. -1 if not one
    int reservedValue = indexInArr(tok->stringval, reservedWords);

    // this is an identifier or an operator
    if (reservedValue == -1)
    {
        // get the op value. -1 if not one
        int opVal = indexInArr(tok->stringval, operators);

        // it's an identifier
        if (opVal == -1)
        {
            tok->tokentype = IDENTIFIERTOK;
            tok->datatype = STRINGTYPE;
        }
        // it's an operator
        else
        {
            tok->tokentype = OPERATOR;
            tok->datatype = INTEGER;
            tok->whichval = opVal;
        }

    }
    // this is a reserved word
    else
    {
        tok->tokentype = RESERVED;
        tok->datatype = INTEGER;
        tok->whichval =  reservedValue;
    }

    return tok;
}

/*
    parse each character and add to string, if space permits.
    this approach parses the entire valid string, but only
    records the first 15 characters.
*/
TOKEN getstring(TOKEN tok)
{
    // remove the opening apostrophe
    // GUARANTEED to be an apostrophe
    getchar();

    // keep track of the character, number of chars recorded
    char c = peekchar();
    int charsAdded = 0;

    while (c != EOF)
    {
        // fetch the character
        c = getchar();
        char cc = peekchar();

        // end parsing this string upon a single closing apostrophe
        if (c == '\'' && cc != '\'')
        {
            break;
        }

        // double apostrophe includes one, and advances the current char
        if (c == '\'' && cc == '\'')
        {
            getchar();
        }

        // add the character if this statement is reached and size permits
        if (charsAdded < 15)
        {
            tok->stringval[charsAdded] = c;
            charsAdded++;
        }

        // progress c
        c = peekchar();
    }

    tok->tokentype = STRINGTOK;
    tok->datatype = STRINGTYPE;
    tok->stringval[charsAdded + 1] = 0;

    return tok;
}

/*  
    construct a string from the special characters,
    then use to compare to operators and delimiters
*/
TOKEN special(TOKEN tok)
{
    // current char, length of string
    int c = peekchar();
    int charsAdded = 0;

    while (c != EOF && CHARCLASS[c] == SPECIAL && charsAdded < 2)
    {
        c = getchar();
        tok->stringval[charsAdded] = c;

        // increment c, charsAdded
        charsAdded++;
        c = peekchar();
    }
    // terminate special string
    tok->stringval[charsAdded + 1] = 0;

    /*  at this point, the collect string may be
            an operator
            a delimiter
        find out which! */
    // get opval, -1 if not an op
    int val = indexInArr(tok->stringval, operators);
    // it's a delimiter.
    if (val == -1)
    {
        val = indexInArr(tok->stringval, delimiters);
        // if this fails, it is a delimter right next to a separate special
        if (val == -1)
        {
            ungetc(tok->stringval[1], stdin);
            tok->stringval[1] = 0;
            val = indexInArr(tok->stringval, delimiters);
        }
        tok->tokentype = DELIMITER;
    }
    // it's an operator!
    else
    {
        tok->tokentype = OPERATOR;
    }

    // fill out remaining fields
    tok->datatype = INTEGER;
    tok->whichval = val;

    return tok;
}

/*
    This struct passes both the parsed number and the digits
    until the decimal point or end of number.
*/
typedef struct ParsedNumber
{
    long num;
    int pow;
    int isDecimal;
} ParsedNumber;

ParsedNumber* parseNum()
{
    // get a parsed num, use talloc so memory is zeroed out
    ParsedNumber* pnum = (ParsedNumber*) talloc(1, sizeof(ParsedNumber));

    // we've allocated a ParsedNUmber. Now fill its bits to the brim!
    char c = peekchar();
    int charval = 0;

    // number of digits collected for the number
    int digitsCollected = 0;
    // number of digits read in, used to calculate the power
    int digitsToDecimal = 0;
    int digitsToStart = 0;
    // booleans to track position in number
    int reachedDecimal = 0;
    int isSignificant = 0;

    // loop through entire number, past decimal point
    while (c != EOF && (CHARCLASS[c] == NUMERIC || (c == '.' && CHARCLASS[peek2char()] == NUMERIC)))
    {
        c = getchar();

        if (c == '.')
        {
            reachedDecimal = 63;
        }
        else
        {
            if (c != '0' && isSignificant == 0)
            {
                isSignificant = 63;
            }

            if (isSignificant == 0)
            {
                digitsToStart++;
            }
            else
            {
                // if there are still digits to consume, add them to the number
                if (digitsCollected < 10)
                {
                    charval = (c - '0');
                    pnum->num = pnum->num * 10 + charval;
                    digitsCollected++;
                }
            }

            if (reachedDecimal == 0)
            {
                digitsToDecimal++;
            }
        }

        c = peekchar();
    }

    //printf("XXX   num: %d, to dec: %d, to start: %d\n", (int)pnum->num, digitsToDecimal, digitsToStart);
    pnum->pow = (digitsToDecimal - digitsToStart) - digitsCollected;
    pnum->isDecimal = reachedDecimal;
    //printf("XXX   num: %d, pow: %d\n", (int)pnum->num, pnum->pow);
    return pnum;
}

/*
    Get and convert unsigned numbers of all types.
    Positioned at start of the number.
    Collect the digits at each possible section of the number:
        mantissa digits | e | -/+ | power digits
*/
TOKEN number(TOKEN tok)
{
    ParsedNumber* number = NULL;
    ParsedNumber* power = NULL;
    char c;

    // parse the number
    number = parseNum();

    // check for a power
    c = peekchar();
    if (c == 'e')
    {
        // remove the exponent delimiter
        getchar();
        
        // check for +/- sign
        c = peekchar();
        int sign = 1;
        if (c == '+' || c == '-')
        {
            getchar();
            if (c == '-')
            {
                sign = -1;
            }
        }

        // parse the power
        power = parseNum();
        // give appropriate sign
        power->num *= sign;

        // now add the power to the number's power
        number->pow += power->num;
    }

    // this is an integer
    if (power == NULL && number->isDecimal == 0)
    {
        // check to see if this integer is out of bounds
        if (number->num > 2147483647)
        {
            printf("Integer number out of range\n");
        }
        tok->tokentype = NUMBERTOK;
        tok->datatype = INTEGER;
        tok->intval = (int)number->num;
    }
    // otherwise, this is a real
    else
    {
        tok->tokentype = NUMBERTOK;
        tok->datatype = REAL;

        // multiply out the number to compute its actual value
        // This is done before bound checks, because double
        // can handle the extra space
        double finalVal = (double)number->num;
        if (number->pow < 100 && number->pow > -100)
        {
            if (number->pow > 0)
            {
                for (int i = 0; i < number->pow; i++)
                {
                    finalVal *= 10;
                }
            }
            else
            {
                for (int i = number->pow; i < 0; i++)
                {
                    finalVal /= 10;
                }
            }
        }
        else
        {
            // give a ridiculously large value to put this number out of range
            // and emit an error message
            finalVal *= 1e100;
        }

        tok->realval = finalVal;

        // check if this floating point is out of range
        if (finalVal > 3.402823e38 || finalVal < 1.175495e-38)
        {
            printf("Floating number out of range\n");
        }
    }

    //printf("XXX   num: %d, pow: %d, is decimal: %d\n", (int)number->num, number->pow, number->isDecimal);
    return tok;
}

    